'use strict';

$(document).ready(function () {
    // Define mobile
    function defineMob() {
        var wrap = $('#header').find('.discription-att');
        var define = detect.parse(navigator.userAgent);
        var os = define.os.family;

        if (os == 'iOS') {
            wrap.addClass('discription-att_ios');
        } else if (os == 'Android') {
            wrap.addClass('discription-att_android');
        }
    }
    defineMob();

  // Mobile menu
  function openMobMenu() {
    var btn = $('#mob-menu');
    var menu = $('#header');

    btn.on('click', function () {
      $(this).toggleClass('mob-menu_active');
      menu.toggleClass('header_active');
    });
  }
  openMobMenu();

  // PopUp
  function openPopUp() {
    $('.js-popup-button').on('click', function () {
      //$('.popup').removeClass('js-popup-show');
      //var popupClass = '.' + $(this).attr('data-popupShow');
      //$(popupClass).addClass('js-popup-show');

      if ($(document).height() > $(window).height()) {
        var scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
        $('html').addClass('noscroll').css('top', -scrollTop);
      }
    });
    closePopup();
  }

  // Close PopUp
  function closePopup() {
    $('.js-close-popup').on('click', function () {
      $('.popup').removeClass('js-popup-show');
      var scrollTop = parseInt($('html').css('top'));
      $('html').removeClass('noscroll');
      $('html, body').scrollTop(-scrollTop);
      return false;
    });

    //$('.popup').on('click', function (e) {
    //  var div = $('.popup__wrap');

    //  if (!div.is(e.target) && div.has(e.target).length === 0) {
    //    $('.popup').removeClass('js-popup-show');
    //    var scrollTop = parseInt($('html').css('top'));
    //    $('html').removeClass('noscroll');
    //    $('html, body').scrollTop(-scrollTop);
    //  }
    //});
  }
  openPopUp();

  // Masked Phone
  $('input[type="tel"]').mask('+7(999)999-99-99');

  // Scroll
  $('#personal-info').find('.table-wrap__inner').scrollbar();
  $('#main').find('.winners-table__wrap').scrollbar();

  // Select
  $('select').select2();

  // Jquery Validate
  //function checkValidate() {
  //  var form = $('form');

  //  $.each(form, function () {
  //    $(this).validate({
  //      ignore: [],
  //      errorClass: 'error',
  //      validClass: 'success',
  //      rules: {
  //        name: {
  //          required: true,
  //          letters: true },

  //        surname: {
  //          required: true,
  //          letters: true },

  //        email: {
  //          required: true,
  //          email: true },

  //        phone: {
  //          required: true,
  //          phone: true },

  //        city: {
  //          required: true,
  //          letters: true },

  //        captcha: {
  //          required: true,
  //          normalizer: function normalizer(value) {
  //            return $.trim(value);
  //          } },

  //        password: {
  //          required: true,
  //          normalizer: function normalizer(value) {
  //            return $.trim(value);
  //          } },

  //        home: {
  //          required: true,
  //          normalizer: function normalizer(value) {
  //            return $.trim(value);
  //          } },

  //        apartment: {
  //          required: true,
  //          normalizer: function normalizer(value) {
  //            return $.trim(value);
  //          } } } });




  //    jQuery.validator.addMethod('letters', function (value, element) {
  //      return this.optional(element) || /^([a-zаА-бб]+)$/i.test(value);
  //    });

  //    jQuery.validator.addMethod('Email', function (value, element) {
  //      return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
  //    });

  //    jQuery.validator.addMethod('Phone', function (value, element) {
  //      return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
  //    });

  //  });

  //}
  //checkValidate();


  // $('form').each(function () {
  //     $(this).validate({
  //         ignore: [],
  //         errorClass: 'error',
  //         validClass: 'success',
  //         //errorElement: "div",
  //         //wrapper: "span",
  //         //errorPlacement: function (error, element) {
  //         //    debugger;
  //         //    if (element.attr("name") == "CityId") {
  //         //        $('#bootstrap_city').html(error);
  //         //        //debugger;
  //         //        //if (element.attr("class") == "select select2-hidden-accessible error") {
  //         //        //    $('#bootstrap_' + element.attr("name")).html(error);
  //         //    } else {
  //         //        error.insertAfter(element);
  //         //    }
  //         //},
  //         rules: {
  //             name: {
  //                 required: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             },
  //             surname: {
  //                 required: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             },
  //             secondname: {
  //                 required: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             },
  //             CityId: {
  //                 required: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             },
  //             email: {
  //                 required: true,
  //                 email: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             },
  //             password: {
  //                 required: true
  //             },
  //             //personalAgreement: {
  //             //    required: true
  //             //},
  //             //rules: {
  //             //    required: true
  //             //},
  //             captcha: {
  //                 required: true
  //             },
  //             filepond: {
  //                 required: true,
  //                 normalizer: function normalizer(value) {
  //                     return $.trim(value);
  //                 }
  //             }
  //         },
  //         messages: {
  //             PhoneNumber: {
  //                 phone: "аЃаКаАаЖаИбаЕ аНаОаМаЕб баЕаЛаЕбаОаНаА"
  //             },
  //             Email: {
  //                 email: "ааВаЕаДаИбаЕ аПбаАаВаИаЛбаНбаЙ email"
  //             },
  //             CityId: {
  //                 required: "абаБаЕбаИбаЕ аГаОбаОаД"
  //             },
  //             rules: {
  //                 required: ''
  //             },

  //             personal: {
  //                 required: ''
  //             }
  //         }
  //     });
  //     jQuery.validator.addMethod('letters', function (value, element) {
  //         return this.optional(element) || /^([a-zаА-бб]+)$/i.test(value);
  //     });

  //     jQuery.validator.addMethod('Email', function (value, element) {
  //         return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
  //     });

  //     jQuery.validator.addMethod('Phone', function (value, element) {
  //         return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
  //     });
  // });

    // Choose prize
    function choosePrize() {
        var select = $('#choose-prize');
        var img = select.closest('.popup__wrap').find('.form__img img');

        select.on('select2:select', function (e) {
            var new_data = e.params.data.element.attributes[0].nodeValue;
            //var data = e.params.data.id;
            img.get(0).src = new_data;
        });
    }
    choosePrize();
});

$('.js-close-popup').click(function() {
  $('div').removeClass('js-popup-show');
})